

修订记录

 

| 日期 | 修订版本 | 修改章节 | 修改描述 |
| ---- | -------- | -------- | -------- |
|      |          |          |          |

缩略语清单： 

| 缩略语 | 英文全名 | 中文解释 |
| ------ | -------- | -------- |
|        |          |          |
|        |          |          |
|        |          |          |
|        |          |          |

# 

# 1   概述

描述本次被测对象变更内容。

# 2   测试版本说明

描述测试版本信息。

| 版本名称 | 测试起始时间 | 测试结束时间 |
| -------- | ------------ | ------------ |
|          |              |              |

描述本次测试的测试环境（包括环境软硬件版本信息，环境组网配置信息, 测试辅助工具等）。

| 硬件型号 | 硬件配置信息 | 备注 |
| -------- | ------------ | ---- |
|          |              |      |
|          |              |      |
|          |              |      |

 

# 3   版本概要测试结论

*本章节概要给出版本测试结论*

# 4   版本详细测试结论

*本章节针对总体测试策略计划的测试内容，给出详细的测试结论。*

## 4.1   特性测试结论



### 4.1.1   继承特性评价

*继承特性进行评价，用表格形式评价，包括特性列表，验证质量评估。*

XXX子系统：特性质量良好

| 序号 | 特性名称    | 特性质量评估                             | 备注 |
| ---- | ----------- | ---------------------------------------- | ---- |
| *1*  | *启动恢复子系统* | *没有新增特性，测试用例执行通过，特性质量良好* |      |
| 2 | 内核子系统 | 轻量系统无合适的公共开发板，该系统内核特性无法验证，风险高<br />小型系统稳定性压力测试存在文件删除，内存泄漏等问题，trace功能缺少指导书，toybox支持的shell命令缺少使用手册，开发者使用难度较大，风险高；<br />标准系统基本功能可用，特性质量良好<br /> | 约束：支持toybox shell命令的rootfs需要刷mksh_rootfs_vfat.img，区别于rootfs_vfat.img<br />问题：<br />[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试I3NT3F内核支持trace功能缺少指导书无法测试，开发者使用难度较大 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BN00)<br/>[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试fs_posix模块nfs用例跑多次会出现不停打印申请内存失败问题](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BL3S)<br/>[【OpenHarmony】【3.0.0.8】【轻内核子系统】dyload_posix模块在removefile的时候出现错误 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BJFU)<br/>[【OpenHarmony】【3.0.0.2】【轻内核子系统】集成测试toybox需提供用户手册，手册中需要将各命令的限制详细说明，以便更好的指导... ](https://gitee.com/openharmony/third_party_toybox/issues/I44YLY)<br/>[【OpenHarmony】【20210701】【轻内核子系统】xts权限用例压测用户态概率异常 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ZJ1D)<br/>[【openHarmony】【3.0 Beta1】【轻内核子系统】集成测试 在执行ActsIpcShmTest.bin脚本，出现大量未释放的共享内存。](https://gitee.com/openharmony/kernel_liteos_a/issues/I47X2Z)<br/>[【OpenHarmony】【1.1.577】【轻内核子系统】集成测试在某个目录下mv一个文件后，再在此目录下创建同名文件并二次mv该文件失败，提示此文件不存在](https://gitee.com/openharmony/third_party_toybox/issues/I44SFO)<br/>[【OpenHarmony】【20210726】【轻内核子系统】集成测试直接执行cat后无法退出，需要重启设备恢复](https://gitee.com/openharmony/third_party_mksh/issues/I42N33) |
| *3*  | 媒体子系统 | 标准系统历史特性（音视频播放、音频管理、相机拍照）验证通过，特性质量良好<br />轻量小型系统无新增特性，测试用例执行通过，特性质量良好 |      |
| 4 | 驱动子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好；<br />标准系统基本功能可用，camera/wifi驱动遗留少量问题; | |
| 5 | 泛sensor服务子系统 | 小型系统没有新增特性，测试用例执行通过，特性质量良好； | |
|6|用户程序框架子系统|小型系统没有新增特性，测试用例执行通过，特性质量良好||
|7|账号子系统|特性基本可用||
|8|杂散子系统|特性基本可用||
|9|元能力子系统|特性基本可用||
|10|事件通知子系统|标准系统没有新增特性，测试用例执行通过，特性质量良好||
|11|窗口子系统|特性基本可用||
|12|电话子系统|特性基本可用||
|12|分布式文件子系统|因为版本差异问题，JS用例无法执行通过，出现用例不执行情况，出现阻塞，影响测试，风险高||
|13|DFX子系统|基本功能可用|native测试用例压测执行不通过，发现遗留问题：1、运行hilog的压测，hilogd异常重启，且hilog命令一直无法使用<br> js测试用例压测执行通过，特性质量良好|
|14| 电源子系统 | 无新增特性，测试用例执行通过，特性质量良好 | |
|15|安全子系统|特性基本可用||
|16|分布式数据库子系统|因为版本差异问题，JS用例无法执行通过，出现用例不执行情况，出现阻塞，影响测试，风险高||
|17|软总线子系统|继承特性：基于wifi的coap发现、自组网和Message/Byte传输<br/>特性质量：<br/>(1) 基本功能可用，3.0.0.9新增多个可靠性问题，其中2个问题初步分析是由短距和HiChain导致，风险高 <br />(2) 代理通道延期，需要在资料里面明确不支持的用法：OpenSession时，参数SessionAttribute的dataType不支持设置为TYPE_MESSAGE|新增问题：1、软总线：[I4BVVW](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVVW),[I4BVYV](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVYV),[I4BW10](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW10),[I4BW3P](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW3P),[I4BZA2]( https://gitee.com/openharmony/communication_dsoftbus/issues/I4BZA2),[I4BZAB](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BZAB),  <br/>短距：[I4BVUL](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVUL) <br/>HiChain：[I4BW6E](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW6E)<br/> 遗留问题(已评估)：软总线 [I4492M](https://gitee.com/openharmony/communication_dsoftbus/issues/I4492M),[I477AF](https://gitee.com/openharmony/communication_dsoftbus/issues/I477AF),[I48YPH](https://gitee.com/openharmony/communication_dsoftbus/issues/I48YPH),[I49HQ3](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HQ3),[I49HN6](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HN6),[I49MP8](https://gitee.com/openharmony/communication_dsoftbus/issues/I49MP8)|
|18|图形子系统|支持vsync,bufferqueue,支持CPU合成特性。提供基础的图形显示能力。在测试demo与系统应用均测试过，界面能正常显示，demo正常运行。特性质量良好||
|19|ACE子系统|JS与NAPI混合开发：特性测试通过，功能正常；<br/>video组件适配：组件可以使用，但进度条不显示；<br/>camera组件支持：组件可以使用，但录像功能异常，音画不同步，有杂音，声音断续卡顿问题。|video组件适配ISSUE单https://gitee.com/openharmony/graphic_standard/issues/I44W7U?from=project-issue；<br />camera组件支持ISSUE单https://gitee.com/openharmony/multimedia_camera_standard/issues/I42TMC?from=project-issue；<br />camera组件支持ISSUE单：https://gitee.com/openharmony/multimedia_media_standard/issues/I42TIB?from=project-issue|

*特性质量评估可选项：特性不稳定，风险高\特性基本可用，遗留少量问题\特性质量良好*

### 4.1.2   新需求评价

*以表格的形式汇总新特性测试执行情况及遗留问题情况的评估,给出特性质量评估结论。*

| lssue号 | 特性名称 | 特性质量评估 | 约束依赖说明 | 备注 |
| ------- | -------- | ------------ | ------------ | ---- |
| I3ZVTJ  |【分布式任务调度子系统】接收远端拉起FA请求，跨设备拉起远端FA|特性基本可用|提供标准系统拉起标准系统远端FA的功能，缺少使用指导书，开发者使用困难，风险高   |     |
| I3ZVTT  |【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-SAMGR模块构建 |特性基本可用|   |  |
| I3ZVT4  |【分布式任务调度子系统】鸿蒙单框架L2分布式能力建设-跨设备绑定元能力|特性基本可用| 提供标准系统绑定标准系统远端元能力的功能。当前缺少使用指导说明，开发者使用较为困难，风险高 |  |
| I49HB0  |【分布式任务调度子系统】启动、绑定能力添加组件visible权限校验| 特性基本可用 |   |  提供标准系统与标准系统之间的组件校验能力    |
| [I3NT3F](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT3F) | 【轻内核子系统】内核支持trace功能 | 缺少使用指导书，开发者使用困难，风险高 |              |      |
| [I3NT63](https://gitee.com/openharmony/kernel_liteos_a/issues/I3NT63) | 【轻内核子系统】pagecache功能完善 | 特性基本可用，遗留少量问题 |              |      |
| [I3ZY55](https://gitee.com/open_harmony/dashboard?issue_id=I3ZY55) | 【Kernel升级适配】Hi3516DV300开发板适配OpenHarmony 5.10内核 | 特性质量良好 | | |
| I40MWK | 构建语言和地区配置和区域显示能力 | 特性基本可用 | | |
| I46W6Q | 【全球化】新增31种语言支持 | 特性基本可用 | | |
| [I3NT3F](https://gitee.com/openharmony/multimedia_media_standard/issues/I42TIB) |【媒体子系统】[Media][媒体录制器]音视频录制及API | 特性基本可用:音视频录制基本功能正常 |              | 遗留问题：<br />音频录制有杂音<br />(https://gitee.com/openharmony/multimedia_media_standard/issues/I4BXWY)|
| [I42TMC](https://gitee.com/openharmony/multimedia_camera_standard/issues/I42TMC) |【媒体子系统】[Camera][相机框架管理]相机录像功能 | 特性不稳定，风险高：基本录像功能可用但存在多个遗留问题 |              | 遗留问题：<br />1.视频录制后播放没声音(https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXY1)<br />2.相机预览时候画面出现闪烁(https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXYV)<br />3.相机录制视频播放速度慢，在图库播放时画面显示不全(https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXDQ)|
| I40OWT |【搜网】Radio状态管理 | 特性基本可用 | modem占用USB接口，导致USB HDC不可用。此为hi3516开发板约束。 | |
| I40OWU |【搜网】搜网注册 | 特性基本可用 | | |
| I40OU3 |【搜网】驻网信息(运营商信息及SPN广播GSM） | 特性基本可用 | | |
| I40OU5 |【搜网】网络状态(漫游、GSM/LTE/WCDMA接入技术） | 特性基本可用 | | |
| I40OU7 |【搜网】信号强度(WCDMA/GSM/LTE) | 特性基本可用 | | |
| I40OTY |【SIM卡】G/U卡文件信息获取、保存 | 特性基本可用 | | |
| I40OWN |【SIM卡】卡状态处理 | 特性基本可用 | | |
| I40OWO |【SIM卡】解锁卡pin、puk | 特性基本可用 | | |
| I40OWM |【SIM卡】卡账户处理 | 特性基本可用 | | |
| I40OSY |【SIM卡】提供拷贝、删除、更新、获取卡短信的能力供电话子系统内部使用 | 特性基本可用 | | |
| I40OUC |【短彩信】提供发送短信功能 | 特性基本可用 | | |
| I40OUF |【短彩信】提供接收短信功能 | 特性基本可用 | | |
| I40OX3 |【短彩信】提供小区广播的能力 | 特性基本可用 | | |
| [I48BOB](https://gitee.com/openharmony/distributeddatamgr_file/issues/I48BOB) |【分布式文件子系统】（需求）JS存储框架开发 | 特性不稳定，风险高 | | XTS自动化用例执行失败，相关JS接口调用失败 |
| [I4A3LY](https://gitee.com/openharmony/distributeddatamgr_file/issues/I4A3LY) |【分布式文件子系统】（需求）JS存储框架挂载能力 - FS Manager | 特性不稳定，风险高 | | XTS自动化用例执行失败，相关JS接口调用失败 |
|         |     【DFX子系统】提供HiAppEvent JS API     |特性基本可用，无遗留问题              |              |      |
|         |     【DFX子系统】提供HiCollie卡死检测框架    |特性不可用，该特性代码未引入到LTS分支              |              |      |
|         |     【DFX子系统】提供HiTrace分布式调用链基础库     |特性不可用，该特性代码未引入到LTS分支              |              |      |
| [I3ZVUD](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVUD) | 【设备管理】提供设备认证、发现和查询的kit接口 | 基本功能可用 | 只支持运行在标准系统下的系统应用调用，其他不支持 | |
| [I3ZVIO](https://gitee.com/open_harmony/dashboard?issue_id=I3ZVIO) | 【设备管理】提供账号无关设备的PIN码认证能力| 基本功能可用：包括PIN码认证用户授权提示、PIN码显示、PIN码输入。PIN码输入弹框显示性能不佳，PIN码输入不流畅易误触 | 只支持运行在标准系统下的设备间认证组网，其他场景不支持| |
| [I40PAV](https://gitee.com/open_harmony/dashboard?issue_id=I40PAV) | 发布开启一个有页面的Ability的WantAgent通知 | 特性基本可用 | | |
| [I40PAW](https://gitee.com/open_harmony/dashboard?issue_id=I40PAW) | 通知删除接口 | 特性基本可用 | | |
| [I40PAX](https://gitee.com/open_harmony/dashboard?issue_id=I40PAX) | 查看Active通知内容和Active通知个数的接口 | 特性基本可用 | | |
| [I40PAY](https://gitee.com/open_harmony/dashboard?issue_id=I40PAY) | 通知取消订阅 | 特性基本可用 | | |
| [I40PAZ](https://gitee.com/open_harmony/dashboard?issue_id=I40PAZ) | 通知订阅 | 特性基本可用 | | |
| [I40PB6](https://gitee.com/open_harmony/dashboard?issue_id=I40PB6) | 应用侧增加slot | 特性基本可用 | | |
| [I40PB7](https://gitee.com/open_harmony/dashboard?issue_id=I40PB7) | 应用侧删除slot | 特性基本可用 | | |
| [I436VL](https://gitee.com/open_harmony/dashboard?issue_id=I436VL) | ces部件化改造 | 未交付 | | |
| [I436VM](https://gitee.com/open_harmony/dashboard?issue_id=I436VM) | ans部件化改造 | 未交付 | | |
| [I40PBC](https://gitee.com/open_harmony/dashboard?issue_id=I40PBC) | 应用侧发布本地分组的普通通知 | 特性基本可用 | | |
| [I40PBF](https://gitee.com/open_harmony/dashboard?issue_id=I40PBF) | 在免打扰模式下发布通知 | 特性基本可用 | | |
| [I40PBG](https://gitee.com/open_harmony/dashboard?issue_id=I40PBG) | 发布开启一个无页面的Ability的wantAgent | 特性基本可用 | | |
| [I40PBH](https://gitee.com/open_harmony/dashboard?issue_id=I40PBH) | 取消WantAgent的实例 | 特性基本可用 | | |
| [I40PBI](https://gitee.com/open_harmony/dashboard?issue_id=I40PBI) | 发布公共事件的WantAgent通知 | 特性基本可用 | | |
| [I40PBM](https://gitee.com/open_harmony/dashboard?issue_id=I40PBM) | 应用侧取消本地通知 | 特性基本可用 | | |
| [I40PBN](https://gitee.com/open_harmony/dashboard?issue_id=I40PBN) | 应用侧发布声音通知 | 特性基本可用 | | |
| [I40PBO](https://gitee.com/open_harmony/dashboard?issue_id=I40PBO) | 应用侧发布振动通知 | 特性基本可用 | | |
| [I40PBP](https://gitee.com/open_harmony/dashboard?issue_id=I40PBP) | 应用侧发布本地有输入框的通知（NotificationUserInput） | 特性基本可用 | | |
| [I40PBD](https://gitee.com/open_harmony/dashboard?issue_id=I40PBD) | 2个不同的slot，加入到同一个Slot组里面 | 特性基本可用 | | |
| [I40PBJ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBJ) | 提供管理角标显示/隐藏的接口 | 特性基本可用 | | |
| [I40PBK](https://gitee.com/open_harmony/dashboard?issue_id=I40PBK) | 提供管理通知许可的接口（设置和查询） | 特性基本可用 | | |
| [I40PBL](https://gitee.com/open_harmony/dashboard?issue_id=I40PBL) | 应用删除SlotGroup | 特性基本可用 | | |
| [I40PBQ](https://gitee.com/open_harmony/dashboard?issue_id=I40PBQ) | 发布带ActionButton的本地通知 | 特性基本可用 | | |
| [I40PBR](https://gitee.com/open_harmony/dashboard?issue_id=I40PBR) | 通知流控处理 | 特性基本可用 | | |
| [I40PBT](https://gitee.com/open_harmony/dashboard?issue_id=I40PBT) | 死亡监听 | 特性基本可用 | | |
| [I40PBU](https://gitee.com/open_harmony/dashboard?issue_id=I40PBU) | 通知shell命令 | 未交付 | | |
| [I40PBB](https://gitee.com/open_harmony/dashboard?issue_id=I40PBB) | 应用侧发布本地图片类型通知 | 特性不稳定 | 依赖多媒体的提供的图片处理能力 | |
| [I40PBS](https://gitee.com/open_harmony/dashboard?issue_id=I40PBS) | 通知图片大小限制 | 特性不稳定 | 依赖多媒体的提供的图片处理能力 | |
| [I4014F](https://gitee.com/open_harmony/dashboard?issue_id=I4014F) | 【帐号子系统】JS API交付，开源+小程序 | 特性基本可用 | | |
| [I43G6T](https://gitee.com/open_harmony/dashboard?issue_id=I43G6T) | WM Client架构演进 | 特性基本可用 | | |
| [I43HMM](https://gitee.com/open_harmony/dashboard?issue_id=I43HMM) | WM Server架构演进 | 特性基本可用 | | |
| [I40OPG](https://gitee.com/open_harmony/dashboard?issue_id=I40OPG) | 【定时服务】非功能性需求 | 特性基本可用 | | |
| [I40OPH](https://gitee.com/open_harmony/dashboard?issue_id=I40OPH) | 【定时服务】时间时区同步 | 特性基本可用 | | |
| [I40OPI](https://gitee.com/open_harmony/dashboard?issue_id=I40OPI) | 【定时服务】定时器功能 | 特性基本可用 | | |
| [I40OPJ](https://gitee.com/open_harmony/dashboard?issue_id=I40OPJ) | 【定时服务】时间时区管理 | 特性基本可用 | | |
| [I436VX](https://gitee.com/open_harmony/dashboard?issue_id=I436VX) | 提供分布式的回调Native接口 | 功能不可用 | 本地ability迁出到远端设备（标准设备之间），只支持FA形式；<br />新增continueAbility接口；<br />存在问题：<br />1，[I4C1B7](https://gitee.com/openharmony/aafwk_standard/issues/I4C1B7?from=project-issue)serviceability 同进程的connect\disconnect\sendrequest不支持<br />2,[I4C18L](https://gitee.com/openharmony/aafwk_standard/issues/I4C18L?from=project-issue)迁出场景无法验收，拉起失败<br />3,[I4C192](https://gitee.com/openharmony/aafwk_standard/issues/I4C192?from=project-issue)ACE2.0 新增ets，需要在config.json中支持失败ets格式，当前LTS无法失败<br />4,[I4C1A4](https://gitee.com/openharmony/aafwk_standard/issues/I4C1A4?from=project-issue)迁移数据传输KV长度JS与native规格不一致 | |
| [I436N0](https://gitee.com/open_harmony/dashboard?issue_id=I436N0) | 支持应用包信息分布式存储能力 | 功能不可用 | 同上 | |
| [I436VH](https://gitee.com/open_harmony/dashboard?issue_id=I436VH) | 创建串行任务分发器，使用串行任务分发器执行任务 | 特性基本可用 | | |
| [I436VI](https://gitee.com/open_harmony/dashboard?issue_id=I436VI) | 创建专有任务分发器，使用专有任务分发器执行任务 | 特性基本可用 | | |
| [I436VJ](https://gitee.com/open_harmony/dashboard?issue_id=I436VJ) | 创建全局并发任务分发器，使用全局并发任务分发器执行任务 | 特性基本可用 | | |
| [I436VK](https://gitee.com/open_harmony/dashboard?issue_id=I436VK) | 创建并发任务分发器，使用并发任务分发器执行任务 | 特性基本可用 | | |
| [I436VT](https://gitee.com/open_harmony/dashboard?issue_id=I436VT) | 安装读取shortcut信息 | 特性基本可用 | 提供了解析config.json中shortcut，存储与读取能力 | |
| I4446U |【安全子系统】【权限管理】标准系统构建分布式权限同步能力 | 基本功能大部分无法验证，风险高 |依赖BMS实现获取应用前后台状态的接口 |BMS未实现获取应用前后台状态的接口导致功能无法完全验证|
| I441K6 |【安全子系统】【权限管理】标准系统构建分布式权限管理主体标识转换能力 | 基本功能正常，稳定性未测试，性能规格缺失 |依赖软总线 |前期软总线不稳定，导致稳定性无法测试，当前软总线已解决，重新验证中；|
| I4447R |【安全子系统】【权限管理】标准系统构建分布式权限检查能力 | 基本功能正常，稳定性未测试，性能规格缺失 |依赖软总线 |前期软总线不稳定，导致稳定性无法测试，当前软总线已解决，重新验证中；|
| I3YQBC |【安全子系统】【权限管理】PMS权限管理支持类型拓展  | 基本功能正常 |||
| [I474ZZ](https://gitee.com/openharmony/distributeddatamgr_appdatamgr/issues/I474ZZ) | 【分布式数据管理】（需求）实现具备CRUD的RDB JS接口 |  特性不稳定，风险高  | | XTS自动化用例执行失败，相关JS接口调用失败 |
|  |【软总线】基于BLE的发现 | 基本功能可用，遗留问题：一般影响较小，[被动发布频繁更新蓝牙广播导致蓝牙资源耗尽，影响需要低功耗的被动发布](https://gitee.com/openharmony/communication_dsoftbus/issues/I4C3BE?from=project-issue) |依赖支持BLE的硬件设备 | 不满足转测条件（无版本，开发9-26提供了一个使用说明，需要自行编译测试版本，且需要dayu特定硬件设备，暂未开展），评估策略：开发提供自测用例和报告|
|  |【软总线】文件传输 | 特性基本可用 | | 0/1级用例执行通过，全量功能、可靠性和稳定性用例进行中|
|  |【软总线】RPC及JS API| 特性基本可用，JS API有遗漏和资料质量差 | | 遗留问题：[I4BTVV](https://gitee.com/openharmony/communication_ipc/issues/I4BTVV),[I4BTW8](https://gitee.com/openharmony/communication_ipc/issues/I4BTW8) <br />不满足转测条件（依赖JS service ability：直接编译出来的Hap无法使用，需要手动修改并重新签名，暂未调通），评估策略：开发自测和依赖RPC的业务联调结果|
|I43I30|【图形子系统】vsync架构演进| 可以支持不同频率vsync的申请；特性基质量良好 | | |
|I43KL7|【图形子系统】合成器支持GPU渲染合成| 可以支持GPU合成，界面渲染正常；可通过日志打印EGL_version为private，确认GPU合成相关信息。命令为：进入hdc shell在hilog 中搜索 “GL version”；此特性不涉及对外接口，涉及GPU渲染，需要在3568开发板上才能生效；特性质量良好 |在3568上生效 | |
|I43V4W|【ACE子系统】支持使用JS开发service ability| 功能不可用 | |ISSUE:https://gitee.com/openharmony/ace_ace_engine/issues/I4BIY8?from=project-issue |
|I43UU4|【ACE子系统】JS应用生命周期补充支持| 功能不可用 | |ISSUE:https://gitee.com/openharmony/ace_ace_engine/issues/I4BIY8?from=project-issue |
|I43V73|【ACE子系统】支持使用JS开发data ability| 测试功能正常可用，质量良好 | | |
|I43UZ0|【ACE子系统】支持系统服务弹窗| 功能正常可用，质量良好 | | |




## 4.2   兼容性测试结论

*补充兼容性测试报告*

## 4.3   安全专项测试结论

评估结论： Redirect<br/>
关键进展：1、本轮测试覆盖OpenHarmony2.2到3.0所有涉及新增需求子系统，其中21个已完成测试，1个子系统还有测试中：应用子系统。当前还有34个安全issues没有关闭。<br/>
          2、编码问题较为突出，静态告警和危险函数均未清零，静态告警主要集中在：元能力、用户程序框架，安全，媒体，电话服务，启动恢复子系统等；危险函数主要集中在安全，分布式软总线，媒体子系统等。<br/>
          3、有10个组件共71个公开漏洞未修复，主要集成在分布式软总线，驱动子系统，安全子系统等<br/>

当前遗留34个issues没有关闭，详细列表如下：
| 标题	   | 问题归属	| 归属子系统 | 状态     | 	URL                       | 
| -------- | ---------- | ---------- | -------- | --------------------------- | 
| 【安全合规】有8个文件存在能调试和可执行的命令	|权限配置|	电话服务子系统	|待办的|	https://gitee.com/openharmony/telephony_core_service/issues/I4BX2M?from=project-issue|
| 【安全合规】有4个文件存在能调试和可执行的命令	|权限配置|	图形子系统	|待办的|	https://gitee.com/openharmony/graphic_standard/issues/I4BX14?from=project-issue|
| 【安全合规】有7个文件存在能调试和可执行的命令	|权限配置|	USB服务子系统|	待办的|	https://gitee.com/openharmony/usb_manager/issues/I4BWWO?from=project-issue|
| 修复 LTS 敏感字扫描问题|	信息泄露	|图形子系统	|进行中|	https://gitee.com/openharmony/ace_engine_lite/issues/I4BVZL?from=project-issue|
| 修改public_config的内容，减少unwind对外暴露的编译选项，避免编译告警	|编译选项	|三方开源软件	|进行中	|https://gitee.com/openharmony/third_party_libunwind/issues/I4BVWC?from=project-issue|
| 【安全合规】curl 7.78.0版本上新扫描出3个CVE漏洞，需要修复|	公开漏洞	|三方开源软件	|修复中|	https://gitee.com/openharmony/third_party_curl/issues/I4BTYJ?from=project-issue|
| 【安全合规】audio_capturer_test和audio_renderer_test两个文件存在能调试和可执行的命令或脚本	|权限配置	|媒体子系统	|待办的	|https://gitee.com/openharmony/multimedia_audio_standard/issues/I4BTK2?from=project-issue|
| destroy_pake_server时没有从堆中将pin等敏感数据清除	|信息泄露	|安全子系统	|待办的|	https://gitee.com/openharmony/security_deviceauth/issues/I4BGP1?from=project-issue|
| import_signed_auth_info_hilink导入的key用于HiChain连接时的身份标志，导入后加密存储在文件中，属于敏感数据，在返回后没有从堆中清除|	信息泄露	|安全子系统	|待办的	|https://gitee.com/openharmony/security_deviceauth/issues/I4BGLS?from=project-issue|
| 3516DV300、3518EV300鸿蒙轻内核子系统安全专项seninfo工具扫描出敏感数据|	信息泄露|	内核子系统	|待办的|	https://gitee.com/openharmony/kernel_liteos_a/issues/I4ARL0?from=project-issue|
| 【版本号：1.1.3】【3516】libjpeg.so扫描出DXX|	公开漏洞	|三方开源软件|	待办的	|https://gitee.com/openharmony/third_party_libjpeg/issues/I4AJM0?from=project-issue|
| 【版本号：1.1.3】【3516；3518】libopenssl_shared.so扫描出DXX	|开源组件	|三方开源软件|	待办的|	https://gitee.com/openharmony/third_party_openssl/issues/I4AJJA?from=project-issue|
| 【版本号：1.1.3】【3516;3518】libavcodec.so组件扫描出是DXX	|开源组件|	DeviceManager组件|	待办的|	https://gitee.com/openharmony/device_hisilicon_third_party_ffmpeg/issues/I4AJI2?from=project-issue|
| 【版本号：1.1.3】【3516】组件libjpeg   9c存在5个未修复漏洞	|公开漏洞|	三方开源软件|	待办的	|https://gitee.com/openharmony/third_party_libjpeg/issues/I4AJFE?from=project-issue|
| 【版本号：1.1.3】组件wpa supplicant 2.9存在3个未修复漏洞	|公开漏洞|	三方开源软件|	待办的|	https://gitee.com/openharmony/third_party_wpa_supplicant/issues/I4AJB9?from=project-issue|
| 【版本号：1.1.3】组件openssl 1.1.1存在14个未修复漏洞	|公开漏洞	|三方开源软件|	待办的|https://gitee.com/openharmony/third_party_openssl/issues/I4AJ9M?from=project-issue|
| 【版本号：1.1.3】组件mbed tls 2.16.8存在4个未修复漏洞	|公开漏洞	|三方开源软件|	待办的|	https://gitee.com/openharmony/third_party_mbedtls/issues/I4AJ6T?from=project-issue|
| 【版本号：1.1.3】组件hostapd 2.9存在2个未修复漏洞	|公开漏洞	|三方开源软件|	待办的|	https://gitee.com/openharmony/third_party_wpa_supplicant/issues/I4AJ4C?from=project-issue|
| 【版本号：1.1.3】组件ffmpeg 4.2.2存在28个未修复漏洞|	公开漏洞	|DeviceManager组件|	待办的	|https://gitee.com/openharmony/device_hisilicon_third_party_ffmpeg/issues/I4AJ1N?from=project-issue|
| 【版本号：1.1.3】组件das u-boot 4.2.2存在4个未修复漏洞	|公开漏洞	|DeviceManager组件	|待办的|https://gitee.com/openharmony/device_hisilicon_third_party_uboot/issues/I4AIYJ?from=project-issue|
| wpa supplicant组件存在6个漏洞未修复|	公开漏洞|	内核子系统|	待办的|	https://gitee.com/openharmony/kernel_liteos_a/issues/I4AIGE?from=project-issue|
|  扫描 OHOS_STD powermgr_power_manager 编码规范	|代码安全|	电源管理子系统	|待办的|	https://gitee.com/openharmony/powermgr_power_manager/issues/I4A6Q6?from=project-issue|
|  扫描 OHOS_STD powermgr_display_manager 编码规范|	代码安全|	电源管理子系统	|待办的|	https://gitee.com/openharmony/powermgr_display_manager/issues/I4A6PN?from=project-issue|
| 扫描 OHOS_STD powermgr_battery_manager 编码规范	|代码安全|	电源管理子系统|	待办的|	https://gitee.com/openharmony/powermgr_battery_manager/issues/I4A6OS?from=project-issue|
| 内存泄漏	|内存泄漏|事件通知子系统	|待办的|	https://gitee.com/openharmony/notification_ces_standard/issues/I49ZAE?from=project-issue|
| napi_value使用不当，存在内存访问空指针风险|	空指针|用户程序框架子系统|	待办的|	https://gitee.com/openharmony/appexecfwk_standard/issues/I49Z7Y?from=project-issue|
| napi_value使用不当，存在内存访问空指针风险|	空指针	|事件通知子系统	|待办的|	https://gitee.com/openharmony/notification_ces_standard/issues/I49Z5M?from=project-issue|
| 编译告警未清零	|代码安全|	驱动子系统	|待办的|	https://gitee.com/openharmony/drivers_peripheral/issues/I48Y8O?from=project-issue|
| 【芯片DTSE】foundation权限设置为500是否合理？|	权限控制	|编译构建子系统	|进行中	|https://gitee.com/openharmony/build_lite/issues/I48MLS?from=project-issue|
| Softbus_server在执行socketfuzz时，出现crash|	内存管理|	分布式软总线子系统|	技术评审中|	https://gitee.com/openharmony/communication_dsoftbus/issues/I480Z1?from=project-issue|
| 【openHarmony】【3.0 Beta1】【轻内核子系统】集成测试 在执行ActsIpcShmTest.bin脚本，出现大量未释放的共享内存。|	内存管理	|内核子系统	|待办的	|https://gitee.com/openharmony/kernel_liteos_a/issues/I47X2Z?from=project-issue|
| 【OpenHarmony 3.0 Beta1】权限校验没有生效，使用测试 bin 直接调用无权限 hap，期望查询失败返回 0，结果查询成功	|权限校验|	用户程序框架子系统	|技术评审中|	https://gitee.com/openharmony/appexecfwk_appexecfwk_lite/issues/I47ETO?from=project-issue|
| 【OpenHarmony 3.0 Beta1】【驱动子系统】不安全函数和安全编译选项|	代码安全	|驱动子系统|	修复中|	https://gitee.com/openharmony/drivers_peripheral/issues/I47DE6?from=project-issue|
| 【OpenHarmony 3.0 Beta1】安全SecBinaryCheck扫描：libappexecfwk_base.z.so与libeventhandler_native.z.so存在stack-protector疑似告警，需要解决	|编译选项	|用户程序框架子系统|	进行中	|https://gitee.com/openharmony/appexecfwk_standard/issues/I479YY?from=project-issue |


## 4.4   稳定性专项测试结论

*当前稳定性专项只开展了开关机和xts压测专项，无设备稳定性问题发生，当前xts压测由于执行一轮全量测试需要的时间过长导致压测轮次数未达标*

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|     开关机测试     | 整机稳定性           | 无不开机、无整机稳定性问题         | 是          |测试结果：多个设备重启之后，hdc无法自动发现重启后的设备，需要重新kill掉hdc才能发现设备                             |   https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT?from=project-issue        |
|标准系统稳定性测试|xts用例压测|单轮压测6小时无稳定性问题，总共3轮压测|是|测试结果：无异常<br>风险：循环压测轮次未达标，单轮压测执行时间过长，当前无法满足循环次数要求</br>||


## 4.5   性能专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：1、静态KPI通过率100%，2、开关机及动态内存整机达标，子系统无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|          |            |          |          |                             |           |
|          |            |          |          |                             |           |

## 4.6   功耗专项测试结论

*以表格形式汇总各专项测试执行情况及遗留问题情况的评估，给出专项质量评估结论*

*要求：无严重问题

| 测试分类 | 测试评估项 | 质量目标 | 是否满足 | 测试结果及关键遗留问题/风险 | 备注issue |
| -------- | ---------- | -------- | -------- | --------------------------- | --------- |
|          |            |          |          |                             |           |
|          |            |          |          |                             |           |

# 5   问题单统计

[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试I3NT3F内核支持trace功能缺少指导书无法测试，开发者使用难度较大 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BN00)

[【openHarmony】【3.0.0.8】【轻内核子系统】集成测试 在执行ActsTimeiTest.bin脚本，发现打印的错误信息单词拼错。](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BLE8)

[【OpenHarmony】【3.0.0.8】【轻内核子系统】集成测试fs_posix模块nfs用例跑多次会出现不停打印申请内存失败问题](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BL3S)

[【OpenHarmony】【3.0.0.8】【轻内核子系统】dyload_posix模块在removefile的时候出现错误 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I4BJFU)

[【WIP】【OpenHarmony】【3.0.0.2】【轻内核子系统】集成测试toybox需提供用户手册，手册中需要将各命令的限制详细说明，以便更好的指导... ](https://gitee.com/openharmony/third_party_toybox/issues/I44YLY)

[【OpenHarmony】【20210701】【轻内核子系统】xts权限用例压测用户态概率异常 ](https://gitee.com/openharmony/kernel_liteos_a/issues/I3ZJ1D)

[【openHarmony】【3.0 Beta1】【轻内核子系统】集成测试 在执行ActsIpcShmTest.bin脚本，出现大量未释放的共享内存。](https://gitee.com/openharmony/kernel_liteos_a/issues/I47X2Z)

[【OpenHarmony】【1.1.577】【轻内核子系统】集成测试在某个目录下mv一个文件后，再在此目录下创建同名文件并二次mv该文件失败，提示此文件不存在](https://gitee.com/openharmony/third_party_toybox/issues/I44SFO)

[【OpenHarmony】【1.1.541】【轻内核子系统】集成测试toybox 命令kill、cp、mv等命令有些操作提示信息不合理，有些操作回显中有e..](https://gitee.com/openharmony/third_party_toybox/issues/I42VH1)

[【OpenHarmony】【20210726】【轻内核子系统】集成测试直接执行cat后无法退出，需要重启设备恢复](https://gitee.com/openharmony/third_party_mksh/issues/I42N33)

[【OpenHarmony】【3.0.0.9】【媒体子系统】视频录制后播放没声音](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXY1)

[【OpenHarmony】【3.0.0.9】【媒体子系统】相机预览时候画面出现闪烁](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXYV)

[【OpenHarmony】【3.0.0.9】【媒体子系统】音频录制后播放有杂音](https://gitee.com/openharmony/multimedia_media_standard/issues/I4BXWY)

[【OpenHarmony】【3.0.0.9】【媒体子系统】相机录制视频播放速度慢，在图库播放时画面显示不全](https://gitee.com/openharmony/multimedia_camera_standard/issues/I4BXDQ)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板camera驱动压测问题](https://gitee.com/openharmony/drivers_framework/issues/I4BWKC)

[【OpenHarmony 3.0.0.9 】【驱动子系统】3516DV300单板ResetDriver接口失败](https://gitee.com/openharmony/drivers_framework/issues/I4BW0G)

[【OpenHarmony 3.0.0.6】【驱动子系统】L1 3516DV300单板liteos版本调用AllocMem接口测试，单板挂死](https://gitee.com/openharmony/drivers_peripheral/issues/I48A2I)

[【OpenHarmony 3.0 Beta1】【驱动子系统】不安全函数和安全编译选项](https://gitee.com/openharmony/drivers_peripheral/issues/I47DE6)

[【OpenHarmony 3.0.0.9】【DFX子系统】3516DV300单板调用hicollie接口失败](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4BX4J)

[【OpenHarmony 3.0.0.9】【DFX子系统】3516DV300单板调用hitrace接口失败](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4BX1X)

[运行hilog的压测，hilogd异常重启，且hilog命令一直无法使用](https://gitee.com/openharmony/hiviewdfx_hilog/issues/I48IM7)

[hdc_std在设备重启之后，需要hdc_std kill才能重新发现设备](https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT)

[【OpenHarmony】 【3.0.0.9】 【分布式数据子系统】 3516DV300单板 执行测试用例](https://gitee.com/openharmony/xts_acts/issues/I4C227)

[【3.0.0.9】【软总线-组网/短距】L2与手机_切换AP场景_发现组网成功率低(91%)，不达98%](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVUL)

[【3.0.0.9】【软总线-组网】L2与手机_L2开关网络自组网成功率97%_失败3次_需分析失败原因](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVVW)

[【3.0.0.9】【软总线-组网】L2与手机_kill L2软总线进程_组网恢复失败](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BVYV)

[【3.0.0.9】【软总线-组网】L2与手机_重启L2_配网自组网失败(ip是通的也能和手机ping通)](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW10)

[【3.0.0.9】【软总线-传输】L2与手机_组网成功状态,opensession失败](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW3P)

[【3.0.0.9】【软总线-组网/HiChain】L2与手机_手机侧循环开关网络_30次左右开始组网失败不再成功](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BW6E)

[【3.0.0.9】【软总线-传输】opensession参数session name不同，opensession成功发送数据软总线client crash ]( https://gitee.com/openharmony/communication_dsoftbus/issues/I4BZA2)

[【3.0.0.9】【软总线-传输】opensession_datatype为Message走代理_软总线进程crash ](https://gitee.com/openharmony/communication_dsoftbus/issues/I4BZAB)

[【2.2 Beta2】【软总线】组网10分钟自动下线规格，实际测试不准确，耗时多了30s左右](https://gitee.com/openharmony/communication_dsoftbus/issues/I4492M)

[【3.0 beta1】【软总线-传输】传输压测：3个线程间隔500ms发送数据_一段时间后大量返回-999（超时）_用例跑完很久_手机侧还在收到数据](https://gitee.com/openharmony/communication_dsoftbus/issues/I477AF)

[【3.0 beta1】【软总线-组网】测试发现和组网性能(循环离网-发现-组网)_110次组网失败3次](https://gitee.com/openharmony/communication_dsoftbus/issues/I48YPH)

[【3.0 beta1】【软总线-发现】测试发现和组网性能(循环离网-发现-组网)_发现耗时999ms异常，开关机配网自组网触发的发现耗时超过500ms的占24%](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HQ3)

[【3.0 beta1】【软总线-组网】开机首次自组网时间比开关网络自组网时间劣化1s（开机场景均值1.4s，开关网络场景均值0.4s）](https://gitee.com/openharmony/communication_dsoftbus/issues/I49HN6)

[【3.0.0.7】【软总线-组网】2个L2 A/B_A开关网络组网变化_B无上下线回调](https://gitee.com/openharmony/communication_dsoftbus/issues/I49MP8)

[【OpenHarmony 3.0.0.9】【hiviewdfx子系统】3516DV300单板调用hicollie接口失败](https://gitee.com/openharmony/hiviewdfx_hicollie/issues/I4BX4J?from=project-issue)

[【OpenHarmony 3.0.0.9】【hiviewdfx子系统】3516DV300单板调用hitrace接口失败](https://gitee.com/openharmony/hiviewdfx_hitrace/issues/I4BX1X?from=project-issue)

[hdc_std在设备重启之后，需要hdc_std kill才能重新发现设备](https://gitee.com/openharmony/developtools_hdc_standard/issues/I4BXYT?from=project-issue)
